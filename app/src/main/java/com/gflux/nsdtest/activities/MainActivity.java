package com.gflux.nsdtest.activities;

import android.app.Activity;
import android.os.Bundle;

import com.gflux.nsdtest.R;
import com.gflux.nsdtest.fragments.ChoiceFragment;

/**
 * @author Kenneth Saey
 * @since 18-05-2015 15:30
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.content, ChoiceFragment.getInstance())
                    .commit();
        }
    }
}
