package com.gflux.nsdtest.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gflux.nsdtest.R;

/**
 * @author Kenneth Saey
 * @since 18-05-2015 15:38
 */
public class ChoiceFragment extends Fragment {

    private TextView labelServer;
    private TextView labelClient;

    public ChoiceFragment() {

    }

    public static ChoiceFragment getInstance() {
        return new ChoiceFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choice, container, false);
        labelServer = (TextView) view.findViewById(R.id.label_server);
        labelClient = (TextView) view.findViewById(R.id.label_client);
        attachListeners();
        return view;
    }

    private void attachListeners() {
        labelServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.content, ServerFragment.getInstance())
                        .addToBackStack("ServerFragment")
                        .commit();
            }
        });
        labelClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.content, ClientFragment.getInstance())
                        .addToBackStack("ClientFragment")
                        .commit();
            }
        });
    }
}
