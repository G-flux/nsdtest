package com.gflux.nsdtest.fragments;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gflux.nsdtest.R;
import com.gflux.nsdtest.communication.ClientMessageListener;
import com.gflux.nsdtest.communication.Server;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * @author Kenneth Saey
 * @since 18-05-2015 16:48
 */
public class ServerFragment extends ListFragment implements ClientMessageListener {

    // NSD stuff
    private static final String DEFAULT_SERVICE_NAME = "NsdTest";
    private static final String SERVICE_TYPE = "_http._tcp.";
    private NsdServiceInfo serviceInfo;
    private String serviceName;
    private ServerSocket serverSocket;
    private int localPort;
    private NsdManager.RegistrationListener registrationListener;
    private NsdManager nsdManager;
    private ServiceState serviceState = ServiceState.UNREGISTERED;
    private boolean shouldUpdateView = false;

    // Communication stuff (Server)
    private ArrayAdapter<String> messageAdapter;
    private Server server;
    private volatile Thread serverThread;

    // Views
    private TextView labelStart;
    private TextView labelInfo;
    private TextView labelStop;

    public ServerFragment() {
    }

    public static ServerFragment getInstance() {
        return new ServerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        messageAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);
        nsdManager = (NsdManager) getActivity().getSystemService(Context.NSD_SERVICE);
        if (nsdManager != null) {
            initializeService();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_server, container, false);
        labelStart = (TextView) view.findViewById(R.id.label_start);
        labelInfo = (TextView) view.findViewById(R.id.label_info);
        labelStop = (TextView) view.findViewById(R.id.label_stop);
        setListAdapter(messageAdapter);
        attachListeners();
        return view;
    }

    @Override
    public void onResume() {
        shouldUpdateView = true;
        super.onResume();
        updateView();
    }

    @Override
    public void onPause() {
        shouldUpdateView = false;
        unregisterService();
        super.onPause();
    }

    private void attachListeners() {
        labelStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerService();
            }
        });
        labelStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unregisterService();
            }
        });
    }

    private void updateView() {
        if (shouldUpdateView) {
            switch (serviceState) {
                case UNREGISTERED:
                    labelStart.setVisibility(View.VISIBLE);
                    labelInfo.setVisibility(View.GONE);
                    labelStop.setVisibility(View.GONE);
                    break;
                case REGISTERING:
                    labelInfo.setText("Registering the service...");
                    labelStart.setVisibility(View.GONE);
                    labelInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.GONE);
                    break;
                case UNREGISTERING:
                    labelInfo.setText("Unregistering the service...");
                    labelStart.setVisibility(View.GONE);
                    labelInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.GONE);
                    break;
                case REGISTERED:
                    labelInfo.setText("Service is running");
                    labelStart.setVisibility(View.GONE);
                    labelInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.VISIBLE);
                    break;
                case REGISTRATION_FAILED:
                    labelInfo.setText("Service registration failed");
                    labelStart.setVisibility(View.VISIBLE);
                    labelInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.GONE);
                    break;
                case UNREGISTRATION_FAILED:
                    labelInfo.setText("Service unregistration failed");
                    labelStart.setVisibility(View.GONE);
                    labelInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
    }

    private void requestUpdateView() {
        if (shouldUpdateView) {
            Handler mainHandler = new Handler(getActivity().getMainLooper());
            Runnable updateViewTask = new Runnable() {
                @Override
                public void run() {
                    updateView();
                }
            };
            mainHandler.post(updateViewTask);
        }
    }

    private void initializeService() {
        try {
            initializeServerSocket();
            initializeRegistrationListener();

            serviceInfo = new NsdServiceInfo();
            serviceInfo.setServiceName(DEFAULT_SERVICE_NAME);
            serviceInfo.setServiceType(SERVICE_TYPE);
            serviceInfo.setPort(localPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initializeServerSocket() throws IOException {
        serverSocket = new ServerSocket(0);
        localPort = serverSocket.getLocalPort();
    }

    private void initializeRegistrationListener() {
        registrationListener = new NsdManager.RegistrationListener() {
            @Override
            public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                Log.e("NsdTest", "Failed to register service: " + errorCode);
                serviceState = ServiceState.REGISTRATION_FAILED;
                requestUpdateView();
            }

            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                Log.e("NsdTest", "Failed to unregister service: " + errorCode);
                serviceState = ServiceState.UNREGISTRATION_FAILED;
                requestUpdateView();
            }

            @Override
            public void onServiceRegistered(NsdServiceInfo serviceInfo) {
                Log.i("NsdTest", "Service registration succeeded: " + serviceInfo.getServiceName());
                serviceState = ServiceState.REGISTERED;
                requestUpdateView();
                serviceName = serviceInfo.getServiceName();
                startServer();
            }

            @Override
            public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
                Log.i("NsdTest", "Service unregistration succeeded: " + serviceInfo);
                serviceState = ServiceState.UNREGISTERED;
                requestUpdateView();
                stopServer();
            }
        };
    }

    private void registerService() {
        if (nsdManager != null) {
            serviceState = ServiceState.REGISTERING;
            updateView();
            nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, registrationListener);
        }
    }

    private void unregisterService() {
        if (nsdManager != null && (serviceState == ServiceState.REGISTERED || serviceState == ServiceState.REGISTERING || serviceState == ServiceState.UNREGISTRATION_FAILED)) {
            serviceState = ServiceState.UNREGISTERING;
            updateView();
            nsdManager.unregisterService(registrationListener);
        }
    }

    private void startServer() {
        Log.i("NsdTest", "Starting the server");
        if(serverThread != null) {
            stopServer();
        }
        server = new Server(getActivity(), serverSocket, this);
        serverThread = new Thread(server);
        serverThread.start();
    }

    private void stopServer() {
        Log.i("NsdTest", "Stopping the server");
        if(serverThread != null) {
            Thread temporaryReference = serverThread;
            serverThread = null;
            if(server != null) {
                server.stop();
            }
            temporaryReference.interrupt();
        }
    }

    @Override
    public void onNewMessage(String message) {
        messageAdapter.add(message);
        getListView().smoothScrollToPosition(messageAdapter.getCount() - 1);
    }

    private enum ServiceState {
        UNREGISTERED, REGISTERING, UNREGISTERING, REGISTERED, REGISTRATION_FAILED, UNREGISTRATION_FAILED;
    }
}
