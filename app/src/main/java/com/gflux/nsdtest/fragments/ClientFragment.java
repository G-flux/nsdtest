package com.gflux.nsdtest.fragments;

import android.app.Fragment;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gflux.nsdtest.R;
import com.gflux.nsdtest.communication.Client;
import com.gflux.nsdtest.communication.ServerResponseListener;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author Kenneth Saey
 * @since 18-05-2015 16:48
 */
public class ClientFragment extends Fragment implements ServerResponseListener {

    // NSD stuff
    private static final String DEFAULT_SERVICE_NAME = "NsdTest";
    private static final String SERVICE_TYPE = "_http._tcp.";
    private NsdManager nsdManager;
    private NsdManager.DiscoveryListener discoveryListener;
    private NsdManager.ResolveListener resolveListener;
    private NsdServiceInfo serviceInfo;
    private int port;
    private InetAddress host;
    private DiscoveryState discoveryState = DiscoveryState.INACTIVE;
    private DetectionState detectionState = DetectionState.INACTIVE;
    private ResolveState resolveState = ResolveState.INACTIVE;
    private boolean shouldUpdateView = false;

    // Communication Stuff (Client)
//    private Socket serverSocket;
//    private Client client;

    // Views
    private TextView labelStart;
    private TextView labelDiscoveryInfo;
    private TextView labelStop;
    private TextView labelDetectionInfo;
    private View sectionService;
    private TextView valueName;
    private TextView valueType;
    private TextView valueHost;
    private TextView valuePort;
    private View sectionMessages;
    private TextView messageHello;
    private TextView messageWorld;
    private TextView messageData;

    public ClientFragment() {
    }

    public static ClientFragment getInstance() {
        return new ClientFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nsdManager = (NsdManager) getActivity().getSystemService(Context.NSD_SERVICE);
        if (nsdManager != null) {
            initializeResolveListener();
            initializeDiscoveryListener();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client, container, false);
        labelStart = (TextView) view.findViewById(R.id.label_start);
        labelDiscoveryInfo = (TextView) view.findViewById(R.id.label_discovery_info);
        labelStop = (TextView) view.findViewById(R.id.label_stop);
        labelDetectionInfo = (TextView) view.findViewById(R.id.label_service_info);
        sectionService = view.findViewById(R.id.section_service);
        valueName = (TextView) view.findViewById(R.id.value_name);
        valueType = (TextView) view.findViewById(R.id.value_type);
        valueHost = (TextView) view.findViewById(R.id.value_host);
        valuePort = (TextView) view.findViewById(R.id.value_port);
        sectionMessages = view.findViewById(R.id.section_messages);
        messageHello = (TextView) view.findViewById(R.id.message_hello);
        messageWorld = (TextView) view.findViewById(R.id.message_world);
        messageData = (TextView) view.findViewById(R.id.message_data);
        attachListeners();
        return view;
    }

    @Override
    public void onResume() {
        shouldUpdateView = true;
        super.onResume();
    }

    @Override
    public void onPause() {
        shouldUpdateView = false;
        stopDiscovery();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        shouldUpdateView = false;
        stopDiscovery();
        super.onDestroy();
    }

    private void attachListeners() {
        labelStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDiscovery();
            }
        });
        labelStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopDiscovery();
            }
        });
        messageHello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("Hello");
            }
        });
        messageWorld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("World");
            }
        });
        messageData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("{\"key1\":1\"key2\":\"value 2\"}");
            }
        });
    }

    private void updateView() {
        if (shouldUpdateView) {
            switch (discoveryState) {
                case INACTIVE:
                    labelStart.setVisibility(View.VISIBLE);
                    labelDiscoveryInfo.setVisibility(View.GONE);
                    labelStop.setVisibility(View.GONE);
                    labelDetectionInfo.setVisibility(View.GONE);
                    break;
                case START_DISCOVERING:
                    labelDiscoveryInfo.setText("Starting discovery...");
                    labelStart.setVisibility(View.GONE);
                    labelDiscoveryInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.GONE);
                    labelDetectionInfo.setVisibility(View.GONE);
                    break;
                case START_DISCOVERY_FAILED:
                    labelDiscoveryInfo.setText("Starting discovery failed...");
                    labelStart.setVisibility(View.VISIBLE);
                    labelDiscoveryInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.GONE);
                    labelDetectionInfo.setVisibility(View.GONE);
                    break;
                case DISCOVERING:
                    labelDiscoveryInfo.setText("Discovering...");
                    labelStart.setVisibility(View.GONE);
                    labelDiscoveryInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.VISIBLE);
                    labelDetectionInfo.setVisibility(View.VISIBLE);
                    break;
                case STOP_DISCOVERING:
                    labelDiscoveryInfo.setText("Stopping discovery...");
                    labelStart.setVisibility(View.GONE);
                    labelDiscoveryInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.GONE);
                    labelDetectionInfo.setVisibility(View.VISIBLE);
                    break;
                case STOP_DISCOVERY_FAILED:
                    labelDiscoveryInfo.setText("Stopping discovery failed...");
                    labelStart.setVisibility(View.GONE);
                    labelDiscoveryInfo.setVisibility(View.VISIBLE);
                    labelStop.setVisibility(View.VISIBLE);
                    labelDetectionInfo.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
            switch (detectionState) {
                case INACTIVE:
                    labelDetectionInfo.setText("No services found");
                    sectionService.setVisibility(View.GONE);
                    sectionMessages.setVisibility(View.GONE);
                    break;
                case SERVICE_FOUND:
                    labelDetectionInfo.setText("Services found");
                    sectionService.setVisibility(View.VISIBLE);
                    sectionMessages.setVisibility(View.VISIBLE);
                    break;
                case SERVICE_LOST:
                    labelDetectionInfo.setText("Service lost");
                    sectionService.setVisibility(View.GONE);
                    sectionMessages.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
            switch (resolveState) {
                case INACTIVE:
                    sectionService.setVisibility(View.GONE);
                    sectionMessages.setVisibility(View.GONE);
                    break;
                case RESOLVING:
                    sectionService.setVisibility(View.VISIBLE);
                    sectionMessages.setVisibility(View.GONE);
                    break;
                case RESOLVE_FAILED:
                    sectionService.setVisibility(View.VISIBLE);
                    sectionMessages.setVisibility(View.GONE);
                    break;
                case RESOLVED:
                    sectionService.setVisibility(View.VISIBLE);
                    sectionMessages.setVisibility(View.VISIBLE);
                    valueName.setText(serviceInfo.getServiceName());
                    valueType.setText(serviceInfo.getServiceType());
                    valueHost.setText(host.getHostAddress());
                    valuePort.setText(String.valueOf(port));
                    break;
                default:
                    break;
            }
        }
    }

    private void requestUpdateView() {
        if (shouldUpdateView) {
            Handler mainHandler = new Handler(getActivity().getMainLooper());
            Runnable updateViewTask = new Runnable() {
                @Override
                public void run() {
                    updateView();
                }
            };
            mainHandler.post(updateViewTask);
        }
    }

    private void initializeResolveListener() {
        resolveListener = new NsdManager.ResolveListener() {
            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                resolveState = ResolveState.RESOLVE_FAILED;
                requestUpdateView();
                Log.e("NsdTest", "Resolve failed: " + errorCode);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo nsdServiceInfo) {
                resolveState = ResolveState.RESOLVED;
                requestUpdateView();
                serviceInfo = nsdServiceInfo;
                port = serviceInfo.getPort();
                host = serviceInfo.getHost();
                stopDiscovery();
            }
        };
    }

    private void initializeDiscoveryListener() {
        discoveryListener = new NsdManager.DiscoveryListener() {
            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                discoveryState = DiscoveryState.START_DISCOVERY_FAILED;
                requestUpdateView();
                nsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                discoveryState = DiscoveryState.STOP_DISCOVERY_FAILED;
                requestUpdateView();
            }

            @Override
            public void onDiscoveryStarted(String serviceType) {
                discoveryState = DiscoveryState.DISCOVERING;
                requestUpdateView();
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                discoveryState = DiscoveryState.INACTIVE;
                detectionState = DetectionState.INACTIVE;
                requestUpdateView();
            }

            @Override
            public void onServiceFound(NsdServiceInfo serviceInfo) {
                if (!serviceInfo.getServiceType().equals(SERVICE_TYPE)) {
                    Log.i("NsdTest", "Unknown service type: " + serviceInfo.getServiceType());
                } else if (serviceInfo.getServiceName().contains(DEFAULT_SERVICE_NAME)) {
                    detectionState = DetectionState.SERVICE_FOUND;
                    resolveState = ResolveState.RESOLVING;
                    requestUpdateView();
                    nsdManager.resolveService(serviceInfo, resolveListener);
                } else {
                    Log.i("NsdTest", "Unknown service: " + serviceInfo.getServiceName());
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo serviceInfo) {
                resolveState = ResolveState.INACTIVE;
                requestUpdateView();
                detectionState = DetectionState.SERVICE_LOST;
            }
        };
    }

    private void startDiscovery() {
        if (nsdManager != null && (discoveryState == DiscoveryState.INACTIVE || discoveryState == DiscoveryState.START_DISCOVERY_FAILED)) {
            discoveryState = DiscoveryState.START_DISCOVERING;
            updateView();
            nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
        }
    }

    private void stopDiscovery() {
        if (nsdManager != null && (discoveryState == DiscoveryState.DISCOVERING || discoveryState == DiscoveryState.STOP_DISCOVERY_FAILED)) {
            discoveryState = DiscoveryState.STOP_DISCOVERING;
            requestUpdateView();
            nsdManager.stopServiceDiscovery(discoveryListener);
        }
    }

    private void sendMessage(String message) {
        Client client = new Client(getActivity(), host, port, message, this);
        new Thread(client).start();
    }

    @Override
    public void onResponse(String response) {
        Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
    }

    private enum DiscoveryState {
        INACTIVE, START_DISCOVERING, START_DISCOVERY_FAILED, DISCOVERING, STOP_DISCOVERING, STOP_DISCOVERY_FAILED;
    }

    private enum DetectionState {
        INACTIVE, SERVICE_FOUND, SERVICE_LOST;
    }

    private enum ResolveState {
        INACTIVE, RESOLVING, RESOLVE_FAILED, RESOLVED;
    }
}
