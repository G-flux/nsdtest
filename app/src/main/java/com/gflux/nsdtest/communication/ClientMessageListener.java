package com.gflux.nsdtest.communication;

/**
 * @author Kenneth Saey
 * @since 19-05-2015 16:38
 */
public interface ClientMessageListener {

    void onNewMessage(String message);
}
