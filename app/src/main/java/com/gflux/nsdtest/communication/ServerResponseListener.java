package com.gflux.nsdtest.communication;

/**
 * @author Kenneth Saey
 * @since 20-05-2015 13:25
 */
public interface ServerResponseListener {

    void onResponse(String response);
}
