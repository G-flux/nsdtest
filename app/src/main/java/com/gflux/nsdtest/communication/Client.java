package com.gflux.nsdtest.communication;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author Kenneth Saey
 * @since 20-05-2015 9:18
 */
public class Client implements Runnable {

    private InetAddress host;
    private int port;
    private String message;
    private Handler mainHandler;
    private ArrayList<ServerResponseListener> listeners;

    public Client(Context context, InetAddress host, int port, String message) {
        this.host = host;
        this.port = port;
        this.message = message;
        listeners = new ArrayList<>();
        mainHandler = new Handler(context.getMainLooper());
    }

    public Client(Context context, InetAddress host, int port, String message, ServerResponseListener listener) {
        this(context, host, port, message);
        addServerResponseListener(listener);
    }

    public void addServerResponseListener(ServerResponseListener listener) {
        listeners.add(listener);
    }

    public void removeServerResponseListener(ServerResponseListener listener) {
        listeners.remove(listener);
    }

    public void clearServerResponseListeners() {
        listeners.clear();
    }

    @Override
    public void run() {
        try {
            Log.i("NsdTest", "Sending a message to " + host + ":" + port);
            Socket socket = new Socket(host, port);
            PrintWriter out = new PrintWriter(new BufferedOutputStream(socket.getOutputStream()));
            out.println(message);
            out.flush();
            new Thread(new ResponseReader(socket)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ResponseReader implements Runnable {

        private Socket serverSocket;
        private BufferedReader inputReader;

        public ResponseReader(Socket serverSocket) {
            this.serverSocket = serverSocket;
            try {
                inputReader = new BufferedReader(new InputStreamReader(this.serverSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            Log.i("NsdTest", "Reading response...");
            try {
                final String response = inputReader.readLine();
                inputReader.close();
                Log.i("NsdTest", "Complete response is: " + response);
                for(final ServerResponseListener listener : listeners) {
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onResponse(response);
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
