package com.gflux.nsdtest.communication;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author Kenneth Saey
 * @since 19-05-2015 15:54
 */
public class Server implements Runnable {

    private ServerSocket serverSocket;
    private ArrayList<ClientMessageListener> listeners;
    private boolean stop;

    private Socket clientSocket;
    private Handler mainHandler;

    private Server(Context context, ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
        listeners = new ArrayList<>();
        mainHandler = new Handler(context.getMainLooper());
    }

    public Server(Context context, ServerSocket serverSocket, ClientMessageListener listener) {
        this(context, serverSocket);
        addClientMessageListener(listener);
    }

    public void addClientMessageListener(ClientMessageListener listener) {
        listeners.add(listener);
    }

    public void removeClientMessageListener(ClientMessageListener listener) {
        listeners.remove(listener);
    }

    public void clearClientMessageListeners() {
        listeners.clear();
    }

    public void stop() {
        stop = true;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted() && !stop) {
            try {
                Log.i("NsdTest", "Listening for incomming connections...");
                clientSocket = serverSocket.accept();
                Log.i("NsdTest", "Incomming connection detected");
                MessageReader messageReader = new MessageReader(clientSocket);
                new Thread(messageReader).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class MessageReader implements Runnable {

        private BufferedReader inputReader;

        public MessageReader(Socket clientSocket) {
            try {
                inputReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            Log.i("NsdTest", "Reading message...");
            try {
                final String message = inputReader.readLine();
                new Thread(new ReplyWriter(clientSocket, "{\"type\":\"RESPONSE\",\"status\":\"ACCEPTED\"}")).start();
                Log.i("NsdTest", "Message received: " + message);
                for (final ClientMessageListener listener : listeners) {
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onNewMessage(message);
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class ReplyWriter implements Runnable {

        private Socket clientSocket;
        private String replyMessage;

        public ReplyWriter(Socket clientSocket, String replyMessage) {
            this.clientSocket = clientSocket;
            this.replyMessage = replyMessage;
        }

        @Override
        public void run() {
            try {
                Log.i("NsdTest", "Sending reply message to " + clientSocket.getInetAddress().getHostAddress() + ":" + clientSocket.getPort() + ": " + replyMessage);
                PrintWriter out = new PrintWriter(new BufferedOutputStream(clientSocket.getOutputStream()));
                out.println(replyMessage);
                out.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
