NSDTest
================

A Network Service Discovery example for Android.

This test includes NSD and client-to-server communication with server response handling.

Development
-----------

Author: [@KennethSaey](https://twitter.com/KennethSaey)

Company: [G-flux](http://g-flux.com)

Editor: Developed in Android Studio 1.2.1.1